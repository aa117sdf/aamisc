#define BOOST_TEST_MODULE politetest
#include "boost/test/unit_test.hpp"

// -- Own headers --

#include "aamisc/polite.hh"

// -- Boost headers --

#include "boost/lexical_cast.hpp"

// -- Standard headers --

#include <string>
#include <sstream>

// -- Derived classes --

// Because Polite is abstract, we need test classes.
// Simple inheritance is always tested
// for multiple inheritance, set this to 1
#define TEST_MULTIPLE_INHERITANCE 1

// simple inheritance
class DerPolite: public aamisc::Polite
{
    public:
    
    DerPolite(): 
        Polite{"testprog", "arg1 arg2 [...]"}, 
        _v{3},
        _arg1{}, _args{}    // single and optionally many positional arguments
    {
        add_option<unsigned int>("val", &_v, 3, "uint value, must be >= 3", 'v');
        add_bool_switch("switch", &_s, "Boolean switch", 's');
        add_posarg("arg1", "Single filename argument");
        add_posarguments("args", "One or many filename arguments");
    }
    
    unsigned int v() const { return _v; }
    bool s() const { return _s; }
    const std::string& arg1() const { return _arg1; }
    const std::vector<std::string>& args() const { return _args; }
    
    virtual
	std::ostream& print_version(std::ostream& out) const override { 
	    out << "Version X.Y" << std::endl;
	    return out;
	}
	
    /// make option_seen() publicly available & hence testable
    bool optseen(const std::string& param) const { return option_seen(param); }
    
    protected:
    
    virtual
    bool check_variables() override final {
        bool ok{ true };
        
        if (option_seen("val")) {
            unsigned int _v = fetch_value<unsigned int>("val");
            if (_v < 3) {
                errors().add_error("val must be >= 3");
                ok = false;
            }
        }
        
        if (option_seen("arg1")) {
            _arg1 = fetch_value<std::string>("arg1");
        } else {
            errors().add_error("arg1 missing");
            ok = false;
        }
        
        if (option_seen("args")) {
            _args = fetch_value<std::vector<std::string>>("args");
        } else {
            errors().add_error("args missing");
            ok = false;
        }
        return ok;
    }
    
    private:

    unsigned int _v; // some value
    bool _s;
    std::string _arg1;
    std::vector<std::string> _args;
    
};  // class DerPolite

#if TEST_MULTIPLE_INHERITANCE

// complex: diamond inheritance
// This could be used if a bunch of related apps use
// a set of common options and then each of them add their own.
// It happened once in the past, hence the test. :-)

// NOTE that g++ 4.9.2-compiled code produces a segfault
// unless the mem-initializers use round parentheses instead of C++11 braces.
class Aopt: public virtual aamisc::Polite {
    public:
    
    Aopt(): aamisc::Polite( "A options help" ), _a(3) {
        add_option<int>("aaa", &_a, 3, "integer value, must be >= 3", 'a');
    }
    
    int a() const { return _a; }
    
    virtual
	std::ostream& print_version(std::ostream& out) const override { 
	    out << "Version X.Y of A" << std::endl;
	    return out;
	}
	
    protected:
    
    virtual
    bool check_variables() override {
        if (option_seen("aaa")) {
            int _a = fetch_value<int>("aaa");
            if (_a < 3) {
                errors().add_error("aaa must be >= 3");
                return false;
            }
            return true;
        }
        return false;
    }

    private:
    
    int _a;
};  // class Aopt

class Bopt: public virtual aamisc::Polite {
    public:
    
    Bopt(): aamisc::Polite( "B options help"), _b(4) {
        add_option<int>("bbb", &_b, 4, "integer value, must be >= 4", 'b');
    }
    
    int b() const { return _b; }
    
	std::ostream& print_version(std::ostream& out) const override { 
	    out << "Version X.Y of B" << std::endl;
	    return out;
	}
	
    protected:
    
    bool check_variables() override {
        if (option_seen("bbb")) {
            int _b = fetch_value<int>("bbb");
            if (_b < 4) {
                errors().add_error("bbb must be >= 4");
                return false;
            }
            return true;
        }
        return false;
    }

    private:
    
    int _b;
};  // class Bopt

class Copt: public Aopt, public Bopt
{
    public:
    
    Copt(): aamisc::Polite("C options help"), Aopt(), Bopt(), _c(5) {
        add_option<int>("ccc", &_c, 5, "integer value, must be >= 5", 'c');
    }
    
    int c() const { return _c; }
    
	std::ostream& print_version(std::ostream& out) const override { 
	    out << "Version X.Y of C" << std::endl;
	    return out;
	}
	
    protected:
    
    bool check_variables() override {
        bool oka = Aopt::check_variables(),
            okb = Bopt::check_variables();  // need side effects in both
        
        if (option_seen("ccc")) {
            int _c = fetch_value<int>("ccc");
            if (_c < 5) {
                errors().add_error("ccc must be >= 5");
                return false;
            }
            return oka && okb;
        }
        return false;
    }

    private:
    
    int _c;
};  // class Copt

#endif

// gets rid of annoying "deprecated conversion from string constant blah blah" warning (StackOverflow tip)
#ifdef __GNUC__
#pragma GCC diagnostic ignored "-Wwrite-strings"
#endif

BOOST_AUTO_TEST_CASE(simple_test)
{
    const int ARGC = 7;
    char *ARGV[] = { "politetest", "-v", "4", "--switch", "firstarg", "secondarg", "thirdarg" };
    
    DerPolite dp;
    bool ok = dp.parse_check(ARGC, ARGV);
    BOOST_CHECK(ok);
    BOOST_CHECK_EQUAL(dp.v(), 4);
    BOOST_CHECK(dp.optseen("val"));   // -v option was given
    BOOST_CHECK(dp.s());
    BOOST_CHECK(dp.optseen("arg1"));
    BOOST_CHECK_EQUAL(dp.arg1(), "firstarg");
    BOOST_CHECK(dp.optseen("args"));
    BOOST_CHECK_EQUAL(dp.args().size(), 2);
    BOOST_CHECK_EQUAL(dp.args().front(), "secondarg");
    BOOST_CHECK_EQUAL(dp.args().back(), "thirdarg");
    
    const int ARGC2 = 4;
    char *ARGV2[] = { "politetest", "-v", "2", "firstarg" };
    DerPolite dp2;  // need new, can't parse twice :-(
    ok = dp2.parse_check(ARGC2, ARGV2);
    BOOST_CHECK(!ok);
    BOOST_CHECK(dp2.optseen("val"));   // -v option was given
    
    std::ostringstream oss;
    dp2.errs().print(oss);
    BOOST_CHECK_EQUAL(oss.str(), "ERROR: val must be >= 3\nERROR: args missing\nERROR: total = 2\n");

    const int ARGC3 = 4;
    char *ARGV3[] = { "politetest", "-s", "firstarg", "secondarg" };
    DerPolite dp3;  // need new again...
    ok = dp3.parse_check(ARGC3, ARGV3);
    BOOST_CHECK(ok);
    BOOST_CHECK(!dp3.optseen("val"));   // -v option was not given
    BOOST_CHECK(dp3.optseen("switch")); // -s option was given
    BOOST_CHECK(!dp3.optseen("nosuch")); // some bogus option...

    std::cerr << "=== Simple help ===" << std::endl;
    dp.print_help(std::cerr);
}

#if TEST_MULTIPLE_INHERITANCE
BOOST_AUTO_TEST_CASE(diamond_test)
{
    const int ARGC = 7;
    char *ARGV[] = { "politetest", "-a", "6", "--bbb", "7", "--ccc", "8" };
    
    Copt cp;
    bool ok = cp.parse_check(ARGC, ARGV);
    BOOST_CHECK(ok);
    BOOST_CHECK_EQUAL(cp.a(), 6);
    BOOST_CHECK_EQUAL(cp.b(), 7);
    BOOST_CHECK_EQUAL(cp.c(), 8);

    // not a real test
    std::cerr << "=== Help and version string tests ===" << std::endl;
    Aopt ap;
    ap.print_help(std::cerr);
    ap.print_version(std::cerr);
    
    Bopt bp;
    bp.print_help(std::cerr);
    bp.print_version(std::cerr);
    
    cp.print_help(std::cerr);
    cp.print_version(std::cerr);

}
#endif

#ifdef __GNUC__
#pragma GCC diagnostic pop
#endif


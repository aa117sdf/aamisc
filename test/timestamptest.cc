#define BOOST_TEST_MODULE timestamptest
#include "boost/test/unit_test.hpp"

// -- own header --

#include "aamisc/timestamp.hh"

// -- standard --

#include <ctime>

BOOST_AUTO_TEST_SUITE(timestampsuite)

BOOST_AUTO_TEST_CASE(timestamp_test)
{
    // Get the current time string with the C time utilities
    time_t now = time(NULL);
    struct tm* timeinfo = localtime(&now);
    char nowstr[80];
    strftime(nowstr, 80, "%Y-%m-%d %H:%M:%S", timeinfo);
    
    // hope that <1 seconds have elapsed
    aamisc::Timestamp ts {} ;
    BOOST_CHECK_EQUAL(ts.str(), nowstr);
}

BOOST_AUTO_TEST_SUITE_END()

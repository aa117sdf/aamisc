#ifndef AAMISC_POLITE_HEADER
#define AAMISC_POLITE_HEADER

// == HEADER polite.hh ==

/// \file 
/// \brief Wrapper around the overengineered boost::program_options
/// \author Andras Aszodi
/// \date 2015-04-28

// -- Own headers --

#include "aamisc/errors.hh"

// -- Boost headers --

#include "boost/program_options.hpp"
namespace bpo = boost::program_options;

// -- Standard headers --

#include <string>
#include <vector>
#include <iostream>

// == Classes ==

namespace aamisc {

/// \brief Program Options LITE base class.
/// The Polite class provides a wrapper over boost::program_options
/// to make your life a bit easier. Functionality includes parsing
/// the default --help and --version options.
/// Applications are supposed to subclass Polite
/// and add their own options and positional arguments in the
/// constructor of the derived class. The parsed values
/// should be stored in the derived class instance and accessed
/// by appropriately named getter methods.
class Polite {
    public:
    
    /// Inits with the options "--help/-h" and "--version".
    /// \param progname The name of the program, will be used in the help line
    /// "Usage: <progname> [options] [arguments]"
    /// \param argnames Optional string describing the positional argnames 
    /// like "infile outfile" or "file1 [ file2 ... ]".
    /// If specified, then it will be used in the "Usage..." line.
    Polite(
        const std::string& progname,
        const std::string& argnames = ""
    );
    
    /// Convenience method to process all command line arguments at once.
    /// This method invokes parse_check(argc, argv) and then does the following:-
    /// If version information was requested, invokes print_version() and then
    /// invokes exit(EXIT_SUCCESS). If help was requested, invokes print_help(std::cout)
    /// and then exit(EXIT_SUCCESS). If parsing fails, then the error
    /// messages are printed to std::cerr prefixed with "ERROR: ", then
    /// print_help(std::cerr) is invoked, then exit(EXIT_FAILURE) is invoked.
    /// If parsing was successful, then the method returns normally
    /// and the program can continue.
    /// \param argc the number of commandline arguments as provided by main()
    /// \param argv the commandline arguments in a C std::string array as provided by main()
    void process_commandline(int argc, char* argv[]);

    /// Parses the command line and checks the variables.
    /// \param argc the number of commandline arguments as provided by main()
    /// \param argv the commandline arguments in a C std::string array as provided by main()
    /// \return /true/ on success, /false/ on error. Upon success,
    /// you may access the variables' values.
    bool parse_check(int argc, char* argv[]);
    
    /// Const access to the internal Errors object.
    const Errors& errs() const { return _errors; }
    
    /// Indicates whether version information should be printed.
    /// \return /true/ if the "--version" option has seen.
    bool version_needed() const { return _vm.count("version") > 0; }
    
    /// Prints the version information.
    /// \param out this is the stream the version text is printed to.
    /// \return /out/ is returned here
    virtual
    std::ostream& print_version(std::ostream& out) const = 0;
    
    /// Indicates whether the help should be printed.
    /// \return /true/ if the "--help" or "-h" option has seen,
    /// or if error(s) occurred.
    bool help_needed() const { return _vm.count("help") > 0; }
    
    /// Prints help, using the facilities provided by boost::program_options
    /// \param out this is the stream the help text is printed to.
    /// \return /out/ is returned here
    virtual
    std::ostream& print_help(std::ostream& out) const;
    
    virtual ~Polite() {}
    
    protected:
    
    /// Add a non-mandatory option.
    /// Derived classes should have members for each option variable,
    /// and then invoke this method in their constructors to set them up.
    /// \param name the 'long' option name like "--opt"
    /// \param optvarp pointer to the data member storing the option value
    /// \param defval default value for the option
    /// \param descr the description of the option
    /// \param nm the 'short' option name like "-o", can be omitted
    template <typename Opttype>
    void add_option(const std::string& name, Opttype* optvarp, const Opttype& defval,
        const std::string& descr, const char nm = '\0')
    {
        std::string oname = (nm == '\0')? name: name + ',' + nm;
        _desc.add_options()
            (oname.c_str(), 
            bpo::value<Opttype>(optvarp),
            /* NOTE: this used to be
             * bpo::value<Opttype>(optvarp)->default_value(defval),
             * but that caused _vm.count("name") > 0
             * even if the parameter was not present on the commandline
             */
            descr.c_str());
        *optvarp = defval;  // init default val here then
    }
    
    /// Add a non-mandatory Boolean switch option.
    /// \param name the 'long' option name like "--opt"
    /// \param varp pointer to the data member storing the option value
    /// \param descr the description of the option
    /// \param nm the 'short' option name like "-o", can be omitted
    void add_bool_switch(const std::string& name, bool* varp,
        const std::string& descr, const char nm = '\0');
    
    /// Add a mandatory option.
    /// Derived classes should have members for each option variable,
    /// and then invoke this method in their constructors to set them up.
    /// \param name the 'long' option name like "--opt"
    /// \param descr the description of the option
    /// \param nm the 'short' option name like "-o", can be omitted
    template <typename Opttype>
    void add_mandatory_option(const std::string& name, const std::string& descr, const char nm = '\0')
    {
        std::string oname = (nm == '\0')? name: name + ',' + nm;
        _desc.add_options()
            (oname.c_str(), 
            bpo::value<Opttype>(),
            descr.c_str());
    }
    
    /// Add a single positional argument.
    /// The derived class should store it as a string.
    /// The order of the positional arguments will be determined by the order
    /// of invoking this method. 
    /// \param name The name of the argument
    /// \param descr The description of the argument
    void add_posarg(
        const std::string& name,
        const std::string& descr
    );

    /// Add zero or more positional arguments under one name.
    /// The derived class should always store these as string vectors.
    /// The order of the positional arguments will be determined by the order
    /// of invoking this method. For the "any number of positional arguments" use case
    /// invoke this method with `howmany` = 0 (the default).
    /// For instance if your program should be invoked with one output file
    /// and one or more input files like `prog outfile infile1 [infile2 ...]` then do this:
    /// `parser.add_posarg("outfile", "Output file name");` followed by
    /// `parser.add_posarguments("infiles", "Input file names");`
    /// and in `check_variables()` then make sure there is at least one input file.
    /// \param name The name of the argument
    /// \param descr The description of the argument
    /// \param howmany The number of positional arguments to be stored under one name.
    ///     By default this is 0, meaning that
    ///     any number of positional arguments may be given. 
    void add_posarguments(
        const std::string& name,
        const std::string& descr,
        unsigned int howmany = 0
    );

    /// Actually parses the command line.
    /// NOTE: You may invoke this method only once, because otherwise
    /// strange things happen (seem to be a "feature" of boost::program_options).
    /// \param argc the length of the argument array, must be >=1
    /// \param argv the arguments, argv[0] is the program name
    /// \return /true/ on success, /false/ on error, including repeated parsing.
    bool perform_parsing(int argc, char* argv[]);
    
    /**
     * Derived classes must override this method so that first an appropriate base class
     * version is invoked to process the base class' commandline variables
     * and then additional processing must be done on the derived class' variables
     * if /true/ was returned, like this:-
     * \verbatim
      bool DerivedOpts::check_variables()
      {
          bool ok = BaseOpts::check_variables();    // if there is one base class
          if (ok) {
              // e.g. check some variables here
              if (option_seen("opt") > 0)
              {
                  int opt = fetch_value<int>("opt");
                  if (opt < 1) opt = 1;
                  // ...
                  ok = true;
              }
              else
              {
                  errors().add_error("opt must be present");
                  ok = false;
            }
          }
          return ok;
      }
      \endverbatim
     */
    virtual
    bool check_variables() = 0;
    
    /// Checks whether an option has been seen.
    /// \param optnm the name of the option.
    /// \return true if the option has been seen on the command line.
    bool option_seen(const std::string& optnm) const { return (_vm.count(optnm) > 0); }
    
    /// Fetches the value of an option.
    /// \param optnm the name of the option.
    /// \return the value of the option.
    template <typename Opttype>
    Opttype fetch_value(const std::string& optnm) const {
        return (_vm[optnm].as<Opttype>());
    }
    
    /// \return Non-const access to the Errors member.
    Errors& errors() { return _errors; }
    
    private:
    
    bpo::options_description _desc, _hidden;
    bpo::positional_options_description _posdesc;
    bpo::variables_map _vm;
    std::string _date, _time;
    Errors _errors;
    bool _parsed;    // apparently you may parse only once
};

}   // namespace aamisc

#endif    // AAMISC_POLITE_HEADER

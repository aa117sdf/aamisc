#ifndef AAMISC_TIMESTAMP_HEADER
#define AAMISC_TIMESTAMP_HEADER

// == Header timestamp.hh ==

/// \file
/// Simple timestamps.
/// \author Andras Aszodi
/// \date 2013-10-26

// -- Standard headers --

#include <string>

namespace aamisc {

// == Class ==

/// Instances of `Timestamp` represent a timestamp using the current system time
/// taken at the moment of creating the object.
class Timestamp {

    public:
    
    /// Creates a timestamp using the current system time.
    /// \param format The format string to be used (see std::put_time).
    /// The default format is ISO8601: "2013-10-26 09:42:33".
    explicit Timestamp(const std::string& format = "%F %T");
    
    /// \return The timestamp string.
    const std::string& str() const { return _ts; }
    
    private:
    
    std::string _ts;
};

}   // namespace aamisc

#endif

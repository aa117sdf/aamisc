#ifndef AAMISC_AAMISC_HEADER
#define AAMISC_AAMISC_HEADER

// == Header aamisc.hh ==

/// \file 
/// AA miscellaneous utilities header.
/// Just includes everything else.
/// \author András Aszódi
/// \date 2015-04-29

#include "aamisc/config.hh"
#include "aamisc/errors.hh"
#include "aamisc/polite.hh"
#include "aamisc/timestamp.hh"

#endif  // AAMISC_AAMISC_HEADER

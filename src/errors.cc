// == MODULE errors.cc ==

// -- Own header --

#include "aamisc/errors.hh"

// == Implementation ==

namespace aamisc {

Errors::Errors(
    const std::string& errprefix,
    const std::string& warnprefix
):
    _errors(), _warnings(),
    _errprefix(errprefix + ": "),
    _warnprefix(warnprefix + ": ")
{
}

void Errors::add_error(const std::string& msg) {
    if (msg != "")
        _errors.push_back(_errprefix + msg);
}

void Errors::add_warning(const std::string& msg) {
    if (msg != "")
        _warnings.push_back(_warnprefix + msg);
}

Errors& Errors::operator+=(const Errors& another) {
    _errors.insert(
        _errors.end(),
        another._errors.begin(),
        another._errors.end()
    );
    _warnings.insert(
        _warnings.end(),
        another._warnings.begin(),
        another._warnings.end()
    );
    return *this;
}

std::ostream& Errors::print(std::ostream& outf, bool warnings) const {
    for (auto err : _errors)
        outf << err << std::endl;
    if (error_count() > 0)
        outf << _errprefix << "total = " << error_count() << std::endl;
    
    if (warnings) {
        for (auto warn : _warnings)
            outf << warn << std::endl;
        if (warning_count() > 0)
            outf << _warnprefix << "total = " << warning_count() << std::endl;
    }
    return outf;
}
    
void Errors::clear() {
    _errors.clear();
    _warnings.clear();
}

}   // namespace aamisc


// == Module timestamp.cc ==

// -- Own header --

#include "aamisc/timestamp.hh"

// -- Standard headers --

#include <sstream>
#include <iomanip>  // std::put_time (C++11)
#include <ctime>
#include <chrono>

// == Implementation ==

namespace aamisc {

Timestamp::Timestamp(const std::string& format) {
    using std::chrono::system_clock;
    
    std::time_t curtime = 
        system_clock::to_time_t(system_clock::now());
    struct std::tm* loctimeptr = std::localtime(&curtime);
    
    // If the GNU std lib has no get_time, put_time
    // this is the case up to and including G++ 4.9.x
#if __GNUC__ <= 4 && !defined(__clang__)
    constexpr unsigned int TBUFLEN{ 128 };
    char timebuf[TBUFLEN];
    strftime(timebuf, TBUFLEN, format.c_str(), loctimeptr);
    _ts = std::string{ timebuf };
#else
    // C++11 - compliant way of doing it
    std::ostringstream oss;
    oss << std::put_time(loctimeptr, format.c_str());
    _ts = oss.str();
#endif
}

} // namespace aamisc

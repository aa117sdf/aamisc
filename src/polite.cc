// == MODULE polite.cc ==

// -- Standard headers --

#include <cstdlib>

// -- Own headers --

#include "aamisc/polite.hh"

namespace aamisc {

// == Implementation ==

// -- Public methods --

Polite::Polite(
    const std::string& progname,
    const std::string& argnames
):
    _desc{
        "Usage: " + progname + " [options]" +
        ((argnames.size() > 0)? " " + argnames : "") + "\nOptions"
    },
    _errors{ }, 
    _parsed{ false }
{
    _desc.add_options()
        ("help,h", "Print this help and exit")
    ;
    _desc.add_options()
        ("version", "Print version information and exit")
    ;
}

void Polite::process_commandline(int argc, char* argv[]) {
    bool parseok = parse_check(argc, argv);
    if (version_needed()) {
        print_version(std::cout);
        std::exit(EXIT_SUCCESS);
    }
    if (help_needed()) {
        print_help(std::cout);
        std::exit(EXIT_SUCCESS);
    }
    if (! parseok) {
        errors().print(std::cerr);
        print_help(std::cerr);
        std::exit(EXIT_FAILURE);
    }
}

void Polite::add_bool_switch(const std::string& name, bool* varp,
    const std::string& descr, const char nm) {
    std::string oname = (nm == '\0')? name: name + ',' + nm;
    _desc.add_options()
        (oname.c_str(), 
        bpo::bool_switch(varp),
        descr.c_str());
}

void Polite::add_posarg(
    const std::string& name,
    const std::string& descr) {
    _hidden.add_options()
        (name.c_str(), bpo::value<std::string>(), descr.c_str())
    ;
    _posdesc.add(name.c_str(), 1);
}

void Polite::add_posarguments(
    const std::string& name,
    const std::string& descr,
    unsigned int howmany
) {
    _hidden.add_options()
        (name.c_str(), bpo::value< std::vector<std::string> >(), descr.c_str())
    ;
    _posdesc.add(name.c_str(), 
        (howmany == 0)? -1: howmany);
}

bool Polite::parse_check(int argc, char* argv[])
{
    bool ok = perform_parsing(argc, argv);
    if (ok)
        ok = check_variables(); // this should be implemented in derived classes
    return ok;
}

std::ostream& Polite::print_help(std::ostream& out) const
{
    out << _desc << std::endl;
    out << "(c) András Aszódi. All rights reserved." << std::endl;
    return out;
}

// -- Protected methods --

bool Polite::perform_parsing(int argc, char* argv[])
{
    if (_parsed)
        return false;
    _parsed = true;
    
    errors().clear();
    bpo::options_description cmdline;
    cmdline.add(_desc);
    cmdline.add(_hidden);
    try {
        _vm.clear();
        bpo::parsed_options parsed = bpo::command_line_parser(argc, argv).
          options(cmdline).allow_unregistered().positional(_posdesc).run();
        bpo::store(parsed, _vm);
        bpo::notify(_vm);
        
        // check if there have been unrecognised options, store them
        std::vector<std::string> unknown = bpo::collect_unrecognized(
            parsed.options, bpo::exclude_positional);
        if (unknown.size() > 0) {
            for (auto unk : unknown) {
                errors().add_error("Unknown parameter: " + unk);
            }
            return false;
        }
        else return true;
    } catch (bpo::error& err) {
        errors().add_error(err.what());
        return false;
    }
}

} // namespace aamisc

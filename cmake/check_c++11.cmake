# Copyright (c) 2013 Nathan Osman

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

# Modifications by Andras Aszodi:
# Simplified the enable_cxx11 logic
# GNU C++: require Version 4.9 or above
# Added the Intel C++ compiler, decent (but not full)  support is from V14.0 = XE2013 SP1

# Determines whether or not the compiler supports C++11
macro(check_for_cxx11_compiler _VAR)
    set(${_VAR})
    if((MSVC AND MSVC10) OR
       (CMAKE_CXX_COMPILER_ID STREQUAL "GNU" AND NOT ${CMAKE_CXX_COMPILER_VERSION} VERSION_LESS 4.9) OR
       (CMAKE_CXX_COMPILER_ID MATCHES "Clang" AND NOT ${CMAKE_CXX_COMPILER_VERSION} VERSION_LESS 3.1) OR
       (CMAKE_CXX_COMPILER_ID MATCHES "Intel" AND NOT ${CMAKE_CXX_COMPILER_VERSION} VERSION_LESS 14.0))
        set(${_VAR} 1)
        message(STATUS "C++11 supported")
    else()
        message(STATUS "C++11 not supported")
    endif()
endmacro()

# Sets the appropriate flag to enable C++11 support
macro(enable_cxx11)
    if (CMAKE_CXX_COMPILER_ID MATCHES "Intel")
        set(C++11_FLAGS "-std=c++11")
    elseif(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
        set(C++11_FLAGS "-std=c++11")
    elseif(CMAKE_CXX_COMPILER_ID MATCHES "Clang")
        set(C++11_FLAGS "-std=c++11 -stdlib=libc++")
    endif()
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${C++11_FLAGS}")
    set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} ${C++11_FLAGS}")
    set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} ${C++11_FLAGS}")
endmacro()


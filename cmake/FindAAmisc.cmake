# Find the AA miscellaneous utilities library
# https://bitbucket.org/aa117sdf/aamisc
#
# 2015-04-29 András Aszódi

# The following variables will be set:
# AAMISC_FOUND: true if found
# AAMISC_INCLUDE_DIR: header location (headers are in ${AAMISC_INCLUDE_DIRS}/aamisc )
# AAMISC_LIBRARY_DIRS: library location
# AAMISC_LIBRARY: the library to be linked
# 
# Define -DAAMISC_ROOT to help CMake find the aamisc library at a non-standard location

set(AAMISC_FOUND FALSE)

# top-level directory location
set(AAMISC_HEADER "/include/aamisc/aamisc.hh")
if (AAMISC_ROOT)
    # force to look in user-defined location
    find_path(AAMISC_TOPLEVEL ${AAMISC_HEADER} PATHS ${AAMISC_ROOT} NO_DEFAULT_PATH)
endif()
if (NOT AAMISC_TOPLEVEL)
    # still not found, look in standard locations
    find_path(AAMISC_TOPLEVEL ${AAMISC_HEADER})
endif()
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(AAMISC_TOPLEVEL DEFAULT_MSG "AAMISC_TOPLEVEL")

if (AAMISC_TOPLEVEL)
    set(AAMISC_INCLUDE_DIR ${AAMISC_TOPLEVEL}/include)
    set(AAMISC_LIBRARY_DIRS ${AAMISC_TOPLEVEL}/lib)
    
    # library location
    if (AAMISC_USE_DYNAMIC_LIBS)
        set(LIBNAMES "aamisc" "libaamisc")
    else()
        set(LIBNAMES "libaamisc.a")
    endif()
    find_library(AAMISC_LIBRARY
        NAMES ${LIBNAMES}
        PATHS ${AAMISC_LIBRARY_DIRS}
    )
    if (AAMISC_LIBRARY)
        set(AAMISC_FOUND TRUE)
    endif()
endif()
mark_as_advanced(AAMISC_INCLUDE_DIRS AAMISC_LIBRARY )
